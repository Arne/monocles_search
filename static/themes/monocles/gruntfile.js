module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      dist: {
        files: {
          'js/monocles.min.js': ['js/monocles.js'],
          'js/searx.min.js': ['js/searx.js']
        }
      }
    },
    jshint: {
      files: ['gruntfile.js', 'js/monocles.js'],
      options: {
        reporterOutput: "",
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    less: {
        development: {
            options: {
                paths: ["less/monocles"]
                //banner: '/*! less/monocles/oscar.css | <%= grunt.template.today("dd-mm-yyyy") %> | https://github.com/asciimoo/searx */\n'
            },
            files: {
              "css/monocles.css": "less/monocles/monocles.less",
              "css/monocles-dark.css": "less/monocles/monocles-dark.less"
            }
        },
        production: {
            options: {
                paths: ["less/monocles"],
                //banner: '/*! less/monocles/oscar.css | <%= grunt.template.today("dd-mm-yyyy") %> | https://github.com/asciimoo/searx */\n',
                cleancss: true
            },
            files: {
              "css/monocles.min.css": "less/monocles/monocles.less",
              "css/monocles-dark.min.css": "less/monocles/monocles-dark.less"
            }
        },
        /*
        // built with ./manage.sh styles
        bootstrap: {
            options: {
                paths: ["less/bootstrap"],
                cleancss: true
            },
            files: {"css/bootstrap.min.css": "less/bootstrap/bootstrap.less"}
        },
        */
    },
    watch: {
        scripts: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'uglify']
        },
        monocles_styles: {
            files: ['less/monocles/**/*.less'],
            tasks: ['less:development', 'less:production']
        },
        bootstrap_styles: {
            files: ['less/bootstrap/**/*.less'],
            tasks: ['less:bootstrap']
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('test', ['jshint']);

  grunt.registerTask('default', ['jshint', 'uglify', 'less']);

  grunt.registerTask('styles', ['less']);

};
