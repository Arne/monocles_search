{% from 'monocles/macros.html' import icon %}

{% set monocles_style = "" %}
{% if preferences.get_value('monocles-style') == 'dark' %}
    {% set monocles_style = "dark-mode" %}
{% endif %}

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" {% if rtl %} dir="rtl" {% endif %} class="{{ monocles_style }}">
<head>
    {% set version = searx_version.split('+') %}
    <meta charset="UTF-8" />
    <meta name="description" content="spot ecloud global, powered by searx" />
    <meta name="keywords" content="spot, ecloud, searx, search, search engine, metasearch, meta search" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="searx/{{ version[0] }}">
    <meta name="referrer" content="no-referrer">
    <meta name="viewport" content="width=device-width, initial-scale=1 , maximum-scale=1.0, user-scalable=1" />
    {% block meta %}{% endblock %}
    <title>{% block title %}{% endblock %}{{ instance_name }}</title>

    <link rel="stylesheet" href="{{ url_for('static', filename='css/select2.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url_for('static', filename='css/monocles.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url_for('static', filename='css/monocles-dark.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url_for('static', filename='css/navbar.css') }}" type="text/css" />
    {% for css in styles %}
        <link rel="stylesheet" href="{{ url_for('static', filename=css) }}" type="text/css" />
    {% endfor %}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ url_for('static', filename='js/html5shiv.min.js') }}"></script>
      <script src="{{ url_for('static', filename='js/respond.min.js') }}"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{ url_for('static', filename='img/favicon.png') }}" />

    {% block styles %}
    {% endblock %}
    {% block head %}
    {% endblock %}

    <link title="{{ instance_name }}" type="application/opensearchdescription+xml" rel="search" href="{{ url_for('opensearch') }}"/>

    <noscript>
        <style type="text/css">
            .tab-content > .active_if_nojs, .active_if_nojs {display: block !important; visibility: visible !important;}
            .margin_top_if_nojs {margin-top: 20px;}
            .hide_if_nojs {display: none !important;overflow:none !important;}
            .disabled_if_nojs {pointer-events: none; cursor: default; text-decoration: line-through;}
        </style>
    </noscript>
</head>

<body>
    {% include 'monocles/components/icons.html' %}

<!--    {% include 'monocles/components/navbar.html' %}-->


  
 <div class="navbar">
  <div class="dropdown">
    <button style="text-align:right;" class="dropbtn">≡
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
        <a href="https://ocean.monocles.eu/">{{_('Ocean')}}</a>
        <a href="https://mail.monocles.eu/">{{_('Mail')}}</a>
        <a href="https://monocles.social/">{{_('Social')}}</a>
        <a href="https://monocles.eu/conversejs">{{_('Chat')}}</a>
        <a href="https://translate.monocles.de/">{{_('Translator')}}</a>
        <a href="https://monocles.live/">{{_('Videos')}}</a>
        <hr class="separator" />
        <a href="https://ocean.monocles.eu/apps/registration/">{{_('Register')}}</a>
        <a href="https://monocles.eu/more/">{{_('about')}}</a>
        <a href="https://monocles.wiki/">{{_('Wiki')}}</a>
        <hr class="separator" />
        <a href="{{ url_for('preferences') }}">{{ _('preferences') }}</a>
    </div>
  </div>
</div>




    {% set inline_search_form = true %}
    <main>

    {% block site_alert_error %}
    {% endblock %}
    {% block site_alert_warning %}
    {% endblock %}
    {% block site_alert_info %}
    {% endblock %}
    {% block site_alert_success %}
    {% endblock %}

    {% block content %}
    {% endblock %}

    <div id="image_view_modal" class="hidden">
        <div class="card-container">
            <div id="image_view_card">
                <button id="close_image_view_modal">{{ icon("close") }}</button>
                <img id="image_view_image" src="">
                <div class="options">
                    <a id="image_view_file_link" class="btn" {% if results_on_new_tab %}target="_blank" rel="noopener noreferrer"{% else %}rel="noreferrer"{% endif %} href="#">{{_("view file")}}</a>
                    <a id="image_view_url_link" class="btn" {% if results_on_new_tab %}target="_blank" rel="noopener noreferrer"{% else %}rel="noreferrer"{% endif %} href="#">{{_("view source")}}</a>
                </div>
            </div>
        </div>
    </div>
<br><br>
<center>
<a href="https://liberapay.com/monocles/donate">Support monocles</a> for a fairer internet!
</center>    
    </main>
    <footer>
        {% block footer %}
        {% endblock %}
        <p class="text-muted">
<center>
  <!--<a href="https://monocles.de/more/"><img align="bottom" width="18em" src="{{ url_for('static', filename='img/drop.svg') }}" alt="more about the project" /></a>
	<a href="https://gofund.me/946170cd"><img align="bottom" width="23em" src="{{ url_for('static', filename='img/heart.png') }}" alt="support for a fair internet" /></a>-->
</center>
            <small>
			Copyright © 2018-2022 <a href="mailto:arne@monocles.de">Arne-Brün Vogelsang</a> <br> 
                <span class="links">
                    <a href="https://monocles.de/impressum/">{{ _('Legal Notice') }}</a> - &nbsp; <a href="https://monocles.de/impressum/#policies-section">{{ _('Privacy') }}</a>
                </span><br>
{{ _('Powered by') }} <a href="{{ brand.DOCS_URL }}">searx</a> - {{ searx_version }}
            </small>
        </p>
    </footer>


    <script src="{{ url_for('static', filename='js/jquery.min.js') }}"></script>
    <script src="{{ url_for('static', filename='js/select2.min.js') }}"></script>
    <script src="{{ url_for('static', filename='js/monocles.min.js') }}"></script>
    {% if autocomplete %}<script src="{{ url_for('static', filename='js/typeahead.bundle.min.js') }}"></script>{% endif %}
    <script src="{{ url_for('static', filename='js/searx.min.js') }}"
            data-method="{{ method or 'POST' }}"
            data-autocompleter="{% if autocomplete %}true{% else %}false{% endif %}"
            data-translations="{{ translations }}"></script>
    {% for script in scripts %}
    <script src="{{ url_for('static', filename=script) }}"></script>
    {% endfor %}
</body>
</html>
